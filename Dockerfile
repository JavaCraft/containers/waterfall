FROM registry.gitlab.com/javacraft/containers/java:17.0.1-1

ARG WATERFALL_VERSION=1.17-454

RUN cd /tmp && \
    curl -L https://papermc.io/api/v2/projects/waterfall/versions/1.17/builds/454/downloads/waterfall-${WATERFALL_VERSION}.jar -o waterfall-${WATERFALL_VERSION}.jar && \
    mkdir -p /opt/waterfall && \
    mv waterfall-${WATERFALL_VERSION}.jar /opt/waterfall/ && \
    ln -s /opt/waterfall/waterfall-${WATERFALL_VERSION}.jar /opt/waterfall/waterfall.jar

WORKDIR /opt/waterfall

RUN echo "end" | java -jar waterfall.jar

ADD ./rootfs/ /

EXPOSE 25577
EXPOSE 25565

ENTRYPOINT [ "/bin/bash", "-c" ]

CMD [ "java -jar waterfall.jar" ]

